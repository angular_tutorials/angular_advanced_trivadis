import { EmployeeFormComponent } from './employee-form/employee-form.component';
import { EmployeeRatingComponent } from './employee-rating/employee-rating.component';

export const components: any[] = [EmployeeFormComponent, EmployeeRatingComponent];

export * from './employee-form/employee-form.component';
export * from './employee-rating/employee-rating.component';
