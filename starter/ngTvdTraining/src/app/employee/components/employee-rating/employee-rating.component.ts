import { Component, forwardRef, Input } from '@angular/core';

import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator
} from '@angular/forms';

@Component({
  selector: 'app-employee-rating',
  template: `
    <button [disabled]="disabled" (click)="decrement()">-</button>
    {{ counterValue }}
    <button [disabled]="disabled" (click)="increment()">+</button>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => EmployeeRatingComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: EmployeeRatingComponent,
      multi: true
    }
  ]
})
export class EmployeeRatingComponent implements ControlValueAccessor, Validator {
  @Input()
  disabled = false;

  // tslint:disable-next-line:variable-name
  private _counterValue = 0;

  @Input()
  get counterValue() {
    return this._counterValue;
  }

  set counterValue(val) {
    this._counterValue = val;
    this.propagateChange(val);
  }

  // Function to call when the rating changes.
  propagateChange: any = () => {};

  // Function to call when the input is touched
  propagateTouched: any = () => {};

  writeValue(value: number): void {
    if (value) {
      this.counterValue = value;
    }
  }

  registerOnChange(fn: (rating: number) => void) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => void) {
    this.propagateTouched = fn;
  }

  // Allows Angular to disable the input.
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  increment() {
    this.counterValue++;
    this.propagateTouched();
  }

  decrement() {
    this.counterValue--;
    this.propagateTouched();
  }

  validate(control: AbstractControl): ValidationErrors | null {
    if (control.value >= 0) {
      return null;
    } else {
      return { ratingNegative: true };
    }
  }
}
