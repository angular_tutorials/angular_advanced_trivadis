import { EmployeeValidators } from './../../validators/employee.validator';
import { Employee } from '../../model/employee.model';

import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  ValidationErrors
} from '@angular/forms';
import { EmployeeService } from '../../services/index';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-form',
  templateUrl: 'employee-form.component.html',
  styleUrls: ['employee-form.component.scss']
})
export class EmployeeFormComponent implements OnInit, OnChanges {
  @Input() employee: Employee;

  @Output() create = new EventEmitter<Employee>();
  @Output() update = new EventEmitter<Employee>();
  @Output() remove = new EventEmitter<Employee>();

  form: FormGroup = this.fb.group({
    firstname: ['', Validators.required],
    lastname: ['', Validators.required],
    email: [
      '',
      {
        validators: Validators.required,
        asyncValidators: this.emailValidator.bind(this),
        updateOn: 'blur'
      }
    ],
    emailConfirm: ['', Validators.required]
  });

  isEdit = false;
  title = 'Create';

  constructor(private fb: FormBuilder, private service: EmployeeService, private router: Router) {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (this.employee && this.employee.id) {
      this.isEdit = true;
      this.title = 'Edit';
      this.form.patchValue({
        firstname: this.employee.firstname,
        lastname: this.employee.lastname,
        email: this.employee.email
      });
    }
  }

  createEmployee() {
    if (this.form.valid) {
      this.create.emit(this.form.value);
    }
  }

  updateEmployee() {
    if (this.form.touched && this.form.valid) {
      this.update.emit({ ...this.employee, ...this.form.value });
    }
  }

  removeEmployee() {
    this.remove.emit({ ...this.employee, ...this.form.value });
  }

  goBack() {
    this.router.navigate(['/employees'], { queryParamsHandling: 'preserve' });
  }

  emailValidator(control: AbstractControl): Promise<ValidationErrors> | null {
    const promise = new Promise<any>((resolve, reject) => {
      const val: string = control.value;
      if (val == null || val.indexOf('@') > 0) {
        resolve(null);
      }
      resolve({ invalidemail: true });
    });
    return promise;
  }
}
