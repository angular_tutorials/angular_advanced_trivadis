import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import * as fromEmployeeContainer from './containers';
import * as fromGuards from './guards';

const routes: Routes = [
  { path: '', component: fromEmployeeContainer.EmployeeListComponent },
  {
    path: 'new',
    component: fromEmployeeContainer.EmployeeComponent
  },
  {
    path: ':employeeId',
    component: fromEmployeeContainer.EmployeeComponent,
    canDeactivate: [fromGuards.EmployeeEditGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule {}
