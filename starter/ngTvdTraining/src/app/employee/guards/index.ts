import { EmployeeEditGuard } from './employee-edit/employee-edit.guard';

export const guards: any[] = [EmployeeEditGuard];

export * from './employee-edit/employee-edit.guard';
