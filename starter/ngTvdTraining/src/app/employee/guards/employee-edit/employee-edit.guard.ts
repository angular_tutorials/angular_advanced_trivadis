import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  CanDeactivate
} from '@angular/router';
import { Observable } from 'rxjs';
import { EmployeeComponent } from '../../containers';

@Injectable({
  providedIn: 'root'
})
export class EmployeeEditGuard implements CanDeactivate<EmployeeComponent> {
  canDeactivate(component: EmployeeComponent): boolean {
    return confirm('Navigate away without saving?');
  }
}
