import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { EmployeeRoutingModule } from './employee-routing.module';

import * as fromEmployeeComponents from './components';
import * as fromEmployeeContainers from './containers';
import * as fromEmployeeGuards from './guards';
import * as fromEmployeePipes from './pipes';
import * as fromEmployeeServices from './services';

@NgModule({
  declarations: [
    ...fromEmployeeContainers.containers,
    ...fromEmployeeComponents.components,
    ...fromEmployeePipes.pipes
  ],
  imports: [CommonModule, EmployeeRoutingModule, SharedModule],
  providers: [...fromEmployeeServices.services, ...fromEmployeeGuards.guards]
})
export class EmployeeModule {}
