import { SelectDirective } from './select/select.directive';

export const directives: any[] = [SelectDirective];

export * from './select/select.directive';
