import {
  Directive,
  Input,
  ElementRef,
  HostBinding,
  Renderer2,
  OnInit,
  HostListener
} from '@angular/core';

@Directive({
  selector: '[appSelect]'
})
export class SelectDirective implements OnInit {
  @Input() defaultColor = 'transparent';
  @Input() highlightColor = 'orange';

  @HostBinding('style.backgroundColor') backgroundColor: string;

  constructor(private elRef: ElementRef, private renderer: Renderer2) {}

  ngOnInit() {
    this.backgroundColor = this.defaultColor;
  }
  @HostListener('mouseenter') mouseover(eventData: Event) {
    this.backgroundColor = this.highlightColor;
  }
  @HostListener('mouseleave') mouseleave(eventData: Event) {
    this.backgroundColor = this.defaultColor;
  }
}
